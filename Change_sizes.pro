#-------------------------------------------------
#
# Project created by QtCreator 2016-07-07T15:29:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Change_sizes
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    addrackdialog.cpp

HEADERS  += widget.h \
    addrackdialog.h

FORMS    += widget.ui \
    addrackdialog.ui
