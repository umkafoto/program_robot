#ifndef ADDRACKDIALOG_H
#define ADDRACKDIALOG_H

#include <QDialog>

namespace Ui {
class addRackDialog;
}

class addRackDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addRackDialog(QWidget *parent = 0);
    ~addRackDialog();

    int x1() const;
    int x2() const;
    int y1() const;
    int y2() const;
    int z() const;
private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::addRackDialog *ui;
};

#endif // ADDRACKDIALOG_H
