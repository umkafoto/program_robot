#include "addrackdialog.h"
#include "ui_addrackdialog.h"

addRackDialog::addRackDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addRackDialog)
{
    ui->setupUi(this);
}

addRackDialog::~addRackDialog()
{
    delete ui;
}

void addRackDialog::on_buttonBox_accepted()
{
    accept();
}

void addRackDialog::on_buttonBox_rejected()
{
    reject();
}

int addRackDialog::x1() const
{
    return ui->x1spinBox->value();
}
int addRackDialog::x2() const
{
    return ui->x2spinBox_2->value();
}
int addRackDialog::y1() const
{
    return ui->y1spinBox_3->value();
}
int addRackDialog::y2() const
{
    return ui->y2spinBox_4->value();
}
int addRackDialog::z() const
{
    return ui->ZspinBox_5->value();
}
