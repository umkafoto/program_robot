#include "widget.h"
#include "ui_widget.h"
#include "addrackdialog.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    QStringList coordinates;
    ui->setupUi(this);
    setWindowTitle("Sizes of rack");
    ui->tableWidget->setColumnCount(5);
    coordinates << "x1" << "x2" << "y1" << "y2" << "z";
    ui->tableWidget->setHorizontalHeaderLabels(coordinates);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_clicked()
{
    int x1;
    int x2;
    int y1;
    int y2;
    int z;
    int res;
    int row;
    addRackDialog pd(this);
    pd.setWindowTitle("Add rack");
    res = pd.exec();
    if(res == QDialog::Rejected)
        return;
    x1 = pd.x1();
    x2 = pd.x2();
    y1 = pd.y1();
    y2 = pd.y2();
    z =  pd.z();
    row = ui->tableWidget->rowCount();
    ui->tableWidget->insertRow(ui->tableWidget->rowCount());
    ui->tableWidget->setItem(row, X1,new QTableWidgetItem(QString::number(x1)));
    ui->tableWidget->setItem(row, X2,new QTableWidgetItem(QString::number(x2)));
    ui->tableWidget->setItem(row, Y1,new QTableWidgetItem(QString::number(y1)));
    ui->tableWidget->setItem(row, Y2,new QTableWidgetItem(QString::number(y2)));
    ui->tableWidget->setItem(row, Z,new QTableWidgetItem(QString::number(z)));

}
